import rouge

def get_all_scores(hypothesis_dict, refrences_dict, mode="avg"):


    if mode == "best":

        apply_best = True
        apply_avg =  False

    else:
        apply_best = False
        apply_avg =  True

    evaluator = rouge.Rouge(metrics=['rouge-n'],
                           max_n=4,
                           limit_length=True,
                           length_limit=100,
                           length_limit_type='words',
                           apply_best= apply_best,
                           apply_avg= apply_avg,
                           alpha=0.5, # Default F1_score
                           weight_factor=1.2,
                           stemming=True)

    all_hypothesis = hypothesis_dict[0]
    
    all_references = refrences_dict[0]
    

    #get scores
    scores = evaluator.get_scores(all_hypothesis, [all_references])
    
    #format
    scores_dict = {}
    for i in range(1,5):

        temp = {'bleu': scores[f'rouge-{i}']['p'],'rouge': scores[f'rouge-{i}']['r'] ,'f1': scores[f'rouge-{i}']['f']}
        scores_dict[f'n-{i}'] = temp
    
    return scores_dict

#tests

'''
r_dict = {0:["hello man","get lost man"]}
h_dict = {0:["hello"]}
scores = get_all_scores(h_dict, r_dict, "best")

print(scores)
'''