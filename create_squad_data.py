#imports
import string
import json
import random
import nltk
import retrieval_models
nltk.download('stopwords')
nltk.download('punkt')
  
from operator import itemgetter, attrgetter
from tqdm import tqdm
from nltk.corpus import stopwords

from evaluator.all_scores import get_all_scores
from simpletransformers.seq2seq import Seq2SeqModel,Seq2SeqArgs

#for top ir answers
def top_reviews_and_scores(question_tokens, review_tokens, inverted_index, reviews, review_ids, select_mode, num_reviews):
    if select_mode == "random":
        scores = list(random.uniform(size=len(reviews)))
    elif select_mode in ["bm25", "indri"]:
        scores = retrieval_models.retrieval_model_scores(question_tokens, review_tokens, inverted_index, select_mode)
    else:
        raise 'Unimplemented Review Select Mode'

    scores, top_review_ids = zip(*sorted(list(zip(scores, review_ids)), reverse=True)) if len(scores) > 0 else ([], [])
    return scores[:num_reviews], top_review_ids[:num_reviews]

#tokenize input text
def tokenize(text):
    punctuations = string.punctuation.replace("\'", '')

    for ch in punctuations:
        text = text.replace(ch, " " + ch + " ")

    tokens = text.split()
    for i, token in enumerate(tokens):
        if not token.isupper():
            tokens[i] = token.lower()
    return tokens


def get_tokens(texts, stop_words):
    text_tokens = [tokenize(r) for r in texts]
    return [[token for token in r if token not in stop_words and token not in string.punctuation] for r in text_tokens]

def find_answer_spans(args, answer_span_lens, answers, context, stop_words, question_tokens):
    context_sentences = nltk.sent_tokenize(context)
    context_text = context
    context = context.split()

    sentence_tokens = get_tokens(context_sentences, stop_words)
    sentence_tokens = list(map(set, sentence_tokens))
    inverted_index = create_inverted_index(sentence_tokens)

    gold_answers_dict = {}
    gold_answers_dict[0] = [answer["answerText"] for answer in answers]

    #use 2 grams at a time to get most similar answers
    answers_snippet_spans_bleu = []
    answers_snippet_spans_rouge = []
    answers_snippet_spans_f1 = []

    for answer_span_len in answer_span_lens:
        char_index = 0
        for word_index in range(len(context)-answer_span_len):
            span = ' '.join(context[word_index: word_index+answer_span_len])
            
            generated_answer_dict = {}
            generated_answer_dict[0] = [span]
            
            #get scores for spans
            scores = get_all_scores(generated_answer_dict, gold_answers_dict)
            
            answers_snippet_spans_bleu.append(((scores['n-2']['bleu']+scores['n-4']['bleu'])/2, {
                'answer_start': char_index,
                'text': span
            }))

            answers_snippet_spans_rouge.append(((scores['n-2']['rouge']+scores['n-4']['rouge'])/2, {
                'answer_start': char_index,
                'text': span
            }))

            answers_snippet_spans_f1.append(((scores['n-2']['f1']+scores['n-4']['f1'])/2, {
                'answer_start': char_index,
                'text': span
            }))

            char_index += (len(context[word_index]) + 1)

    answers_sentence_ir = []
    answers_sentence_bleu = []
    answers_sentence_rouge = []
    answers_sentence_f1 = []


    _, top_sentences_ir = top_reviews_and_scores(
        set(question_tokens),
        sentence_tokens,
        inverted_index,
        context_sentences,
        context_sentences,
        'bm25',
        args["span_max_num"]
    )

    for sentence in top_sentences_ir:
        idx = context_text.find(sentence)
        if idx >= 0:
            answers_sentence_ir.append({
                'answer_start': idx,
                'text': sentence
            })

    for sentence in context_sentences:

        #get scores for answer sentences
        scores = get_all_scores({0: [sentence]}, gold_answers_dict)

        idx = context_text.find(sentence)
        if idx >= 0:
            answers_sentence_bleu.append(((scores['n-2']['bleu']+scores['n-4']['bleu'])/2, {
                'answer_start': idx,
                'text': sentence
            }))
            answers_sentence_rouge.append(((scores['n-2']['rouge']+scores['n-4']['rouge'])/2, {
                'answer_start': idx,
                'text': sentence
            }))

            answers_sentence_f1.append(((scores['n-2']['f1']+scores['n-4']['f1'])/2, {
                'answer_start': idx,
                'text': sentence
            }))

    answers_sentence_bleu = [i[1] for i in sorted(answers_sentence_bleu, reverse=True, key=itemgetter(0))[:args["span_max_num"]]]
    answers_sentence_rouge = [i[1] for i in sorted(answers_sentence_rouge, reverse=True, key=itemgetter(0))[:args["span_max_num"]]]
    answers_sentence_f1 = [i[1] for i in sorted(answers_sentence_f1, reverse=True, key=itemgetter(0))[:args["span_max_num"]]]
    answers_snippet_spans_bleu = [i[1] for i in sorted(answers_snippet_spans_bleu, reverse=True, key=itemgetter(0))[:args["span_max_num"]]]
    answers_snippet_spans_rouge = [i[1] for i in sorted(answers_snippet_spans_rouge, reverse=True, key=itemgetter(0))[:args["span_max_num"]]]
    answers_snippet_spans_f1 = [i[1] for i in sorted(answers_snippet_spans_f1, reverse=True, key=itemgetter(0))[:args["span_max_num"]]]

    return answers_snippet_spans_bleu, answers_snippet_spans_rouge, answers_snippet_spans_f1, answers_sentence_ir, answers_sentence_bleu, answers_sentence_rouge, answers_sentence_f1


def create_inverted_index(review_tokens):
    term_dict = {}
    # TODO: Use actual review IDs
    for doc_id, tokens in enumerate(review_tokens):
        for token in tokens:
            if token in term_dict:
                if doc_id in term_dict[token]:
                    term_dict[token][doc_id] += 1
                else:
                    term_dict[token][doc_id] = 1
            else:
                term_dict[token] = {doc_id: 1}
    return term_dict


model_args = Seq2SeqArgs()
model_args.max_length=100
model_args.silent = True

# Initialize a Seq2SeqModel for English to German translation
model = Seq2SeqModel(
    encoder_decoder_type="bart",
    encoder_decoder_name="facebook/bart-large-cnn",
    use_cuda=False,
)

def summarize_review(review):
    summ_review = model.predict([review])[0]
    return summ_review


def main(args):

    stop_words = set(stopwords.words('english'))
    answer_span_lens = [10, 20]
    
    wfp = open(args["output_file"], 'w')
    rfp = open(args["input_file"], 'r')

    DATA = []

    for line in tqdm(rfp):
        row = json.loads(line)
        DATA.append(row)

    print(f"total samples {len(DATA)}")
    
    DATA = DATA[40000:args["num_samples"]]

    for row in tqdm(DATA):

    
        if row["is_answerable"] == 1:
            reviews = row["review_snippets"]
            
            context = ""
            for review in reviews:
                review_summ = summarize_review(review)
                context += ' ' + review_summ

            answers = row["answers"]

            question_text = row["questionText"]
            question_tokens = tokenize(question_text)

            answers_snippet_spans_bleu, answers_snippet_spans_rouge, answers_snippet_spans_f1, answers_sentence_ir, answers_sentence_bleu, answers_sentence_rouge, answers_sentence_f1 = find_answer_spans(args, answer_span_lens, answers, context, stop_words, question_tokens)

            qas = [{
                'id': row["qid"],
                'is_impossible': False,
                'question': row["questionText"],
                'answers_snippet_spans_bleu': answers_snippet_spans_bleu,
                'answers_snippet_spans_rouge': answers_snippet_spans_rouge,
                'answers_snippet_spans_f1': answers_snippet_spans_f1,
                'answers_sentence_ir': answers_sentence_ir,
                'answers_sentence_bleu': answers_sentence_bleu,
                'answers_sentence_rouge': answers_sentence_rouge,
                'answers_sentence_f1': answers_sentence_f1,
                'human_answers': [answer["answerText"] for answer in answers],
            }]

            wfp.write(json.dumps({
                'context': context,
                'qas': qas,
            }) + '\n')
    wfp.close()

args = {"input_file":"test-qar_all.jsonl","output_file":"test-qar_squad_all_summ_60000.jsonl","span_max_num":5, "num_samples":63000}
main(args)
